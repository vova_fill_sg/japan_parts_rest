from django.http import JsonResponse, HttpResponse
import mysql.connector
import datetime
from PIL import Image
from .models import *
import ftplib
from os import remove
from time import sleep
import threading


def db_open():
    cnx = mysql.connector.connect(user='zelenuic_japan', password='japan2011parts',
                                      host='zelenuic.mysql.ukraine.com.ua',
                                      database='zelenuic_japan')
    cursor = cnx.cursor()
    return cnx, cursor


def db_close(connection):
    connection.close()


def get_cats_from_db():
    try:
        cnx, cursor = db_open()
        query = ("SELECT cat_id, cat_name, cat_parent FROM parts_mt_cats WHERE 1 ORDER BY cat_parent asc;")
        cursor.execute(query)
        for cat_id, cat_name, cat_parent in cursor:
            print(cat_id, cat_name, cat_parent)
            if cat_id == 0 or len(Category.objects.filter(name=cat_name)) > 0:
                continue
            a = Category()
            a.name = cat_name
            a.uid = cat_id
            if cat_parent != 0:
                try:
                    a.parent_cat = Category.objects.get(uid=cat_parent)
                except:
                    continue
            a.save()
        db_close(cnx)
    except:
        pass


def get_categories(request):
    get_cats_from_db()
    categories = []
    a = Category.objects.prefetch_related("cat").all()
    for i in a:
        cats_in = i._prefetched_objects_cache["cat"]
        if cats_in:
            categories.append({i.name: [{j.name:j.uid} for j in cats_in]})
    return JsonResponse(categories, safe=False)

def add(request):    
    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    data = request.POST

    cnx, cursor = db_open()
    cursor.execute("SELECT MAX(link_id) as link_id FROM parts_mt_links WHERE 1;")
    max_id = cursor.next()[0]
    this_id = max_id + 1
    cat_id = Category.objects.get(name=data["cat"]).uid
    obj = {
        "name" : data["name"],
        "alias": "{id}-{alias}".format(id=this_id, alias=data["alias"]),
        "desc": data["desc"],
        "user_id": 42,
        "link_hits": 0,
        "link_votes": 0,
        "link_rating": 0,
        "link_featured": 0,
        "link_published": 0,
        "link_template": "",
        "attribs": "use_map=show_map=map=show_print=show_recommend=show_rating=show_review=show_visit=show_contact=show_report=show_ownerlisting=",
        "metakey": data["name"],
        "metadesc": "",
        "ordering": "999",
        "link_created": dt,
        "publish_up": "2015-07-14 08:27:48",
        "link_modified": dt,
        "address": "",
        "city": "",
        "state": "",
        "country": "",
        "postcode": data["code"],
        "telephone": data["telephone"],
        "fax": data["avail"],
        "email": "",
        "website": "",
        "price": data["price"],
        "lat": "0",
        "lng": "0",
        "zoom": "0"
    }
    query = """INSERT INTO parts_mt_links (link_name, alias, link_desc, user_id, link_hits, link_votes, link_rating, link_featured,
                link_published, link_approved, link_template, attribs, metakey, metadesc, ordering, link_created, publish_up,
                link_modified, address, city, state, country, postcode, telephone, fax, email, website, price, lat, lng, zoom) VALUES (
                "{name}", "{alias}", "{desc}", "{user_id}", "{link_hits}", "{link_votes}", "{link_rating}", "{link_featured}",
                "{link_published}", "{link_approved}", "{link_template}", "{attribs}", "{metakey}", "{metadesc}", "{ordering}", "{link_created}", "{publish_up}",
                "{link_modified}", "{address}", "{city}", "{state}", "{country}", "{postcode}", "{telephone}", "{fax}", "{email}", "{website}", "{price}", "{lat}", "{lng}", "{zoom}");""".format(name= data["name"],
        alias="{id}-{alias}".format(id=this_id, alias=data["alias"]),
        desc=data["desc"],
        user_id=42,
        link_hits=0,
        link_votes=0,
        link_rating=0,
        link_featured=0,
        link_published=1,
        link_template="",
        attribs="use_map=show_map=map=show_print=show_recommend=show_rating=show_review=show_visit=show_contact=show_report=show_ownerlisting=",
        metakey=data["name"],
        metadesc="",
        ordering="999",
        link_created=dt,
        publish_up="2015-07-14 08:27:48",
        link_modified=dt,
        address="",
        city="",
        state="",
        country="",
        postcode=data["code"],
        telephone=data["telephone"],
        fax=data["avail"],
        email="",
        website="",
        price=data["price"],
        lat= "0", lng= "0",zoom="0", link_approved="1")

    cursor.execute(query)
    query = "INSERT INTO parts_mt_cl (link_id, cat_id, main) VALUES ({link_id}, {cat_id}, {main});".format(link_id=this_id,
                                                                                                           cat_id=cat_id,
                                                                                                           main="1")
    cursor.execute(query)
    

    query = "SELECT MAX(img_id) as id FROM parts_mt_images WHERE 1;"
    cursor.execute(query)
    img_id = int(cursor.fetchone()[0]) + 1
    img = Image.open('name.jpg')

    img_big = img.resize((1843, 1382), Image.ANTIALIAS)
    img_big.save('big.jpg')
    send_img('big.jpg', "{}.jpg".format(img_id), r"/www/components/com_mtree/img/listings/m/")
    send_img('big.jpg', "{}.jpg".format(img_id), r"/www/components/com_mtree/img/listings/o/")
    
    img_sm = img.resize((100, 100), Image.ANTIALIAS)
    img_sm.save('sm.jpg')
    send_img('sm.jpg', "{}.jpg".format(img_id), r"/www/components/com_mtree/img/listings/s/")
    
    img.save('name.jpg')
    
    query = "INSERT INTO parts_mt_images (link_id, filename, ordering) VALUES ({}, {}, {});".format(this_id, "'{}.jpg'".format(img_id), "1");
    cursor.execute(query)

    db_close(cnx)

    clean()

def add_part(request):
    f = request.FILES['file']
    with open('name.jpg', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    thread = threading.Thread(target=add, args=[request])
    thread.start()
    return HttpResponse("")


def clean():
    sleep(5)
    try:
        remove('big.jpg')
        remove('sm.jpg')
        remove('name.jpg')
    except:
        clean()


def send_img(file_name, img_name, path):
    print("sending {} to {}".format(file_name, path))
    try:
        session = ftplib.FTP('zelenuic.ftp.ukraine.com.ua','zelenuic_japan','3Br73y0RZd')
        session.cwd(path)
        file = open(file_name,'rb')
        session.storbinary('STOR {}'.format(img_name), file)
        file.close()
        session.quit()
    except Exception as e:
        print(e)

def was(request, word):
    print(word)
    return HttpResponse("")

def accept(request):
    print(request.FILES)
    return HttpResponse("")
