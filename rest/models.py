from django.db import models

class Category(models.Model):
    uid = models.IntegerField()
    name = models.CharField(max_length=30)
    parent_cat = models.ForeignKey("self", blank=True, null=True, related_name="cat")

    def __str__(self):
        return self.name