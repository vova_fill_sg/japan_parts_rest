"""japan_parts_rest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt


from rest.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^get_cats/', get_categories),
    url(r'^add_part/', csrf_exempt(add_part)),
    url(r'^was/(?P<word>[a-zA-Z])', csrf_exempt(was)),
    url(r'^accept_file/', csrf_exempt(accept)),

]
